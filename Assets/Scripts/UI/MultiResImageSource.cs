﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public struct ImageSourceInfo {
	public int resolution;
	public Sprite sprite;
}

[RequireComponent(typeof(Image))]
public class MultiResImageSource : MonoBehaviour {
	Image imgComp;

	public List<ImageSourceInfo> sources;

	void Start () {
		imgComp = GetComponent<Image> ();
		// get the highest level that is just below the screen width
		var level = sources
			.Where(s => s.resolution < Screen.width)
			.Max (s => s.resolution);
		var source = sources.FirstOrDefault (s => s.resolution == level);
		if (source.sprite != null) {
			imgComp.sprite = source.sprite;
			Debug.Log ("Assigning level " + source.sprite.name);
		}
	}
}
	